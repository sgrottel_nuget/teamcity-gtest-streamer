//
// TeamCity GoogleTest streamer adapter
// enables test event streaming report
//
// Based in large potions on the adapter written by JetBrains
// https://github.com/JetBrains/teamcity-cpp/blob/master/common/teamcity_messages.cpp
// Their code is available under the Apache License v2.0
//
// This derived file is:
// Copyright 2018 S. Grottel (http://www.sgrottel.de)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#if (defined(WIN32) || defined(_WIN32) || defined(__WIN32)) && !defined(__CYGWIN__) && !defined(__MINGW32__)
#define TEAMCITY_GTEST_STREAMER_WINDOWS 1
#else
#undef TEAMCITY_GTEST_STREAMER_WINDOWS
#endif

#include <string>
#include <iostream>
#include <gtest/gtest.h>

namespace GTest {

	/// Simple adapter translating GTest events to TeamCity messages
	class TeamCityAdapter : public ::testing::EmptyTestEventListener {
		/// output stream
		std::ostream *m_out;
		/// TeamCity flowid
		std::string m_flowid;

		/// Implementation copied from
		/// https://github.com/JetBrains/teamcity-cpp/blob/master/common/teamcity_messages.cpp
		/// changed: static, inline, ifdef
		static inline std::string getFlowIdFromEnvironment() {
#ifdef TEAMCITY_GTEST_STREAMER_WINDOWS
			char *flowId = NULL;
			size_t sz = 0;
			std::string result;
			if (!_dupenv_s(&flowId, &sz, "TEAMCITY_PROCESS_FLOW_ID")) {
				result = flowId != NULL ? flowId : "";
				free(flowId);
			}

			return result;
#else
			const char *flowId = getenv("TEAMCITY_PROCESS_FLOW_ID");
			return flowId == NULL ? "" : flowId;
#endif
		}

	public:

		/// Implementation copied from
		/// https://github.com/JetBrains/teamcity-cpp/blob/master/common/teamcity_messages.cpp
		/// changed: static, inline, ifdef
		static bool underTeamcity() {
#ifdef TEAMCITY_GTEST_STREAMER_WINDOWS
			char *teamCityProjectName = 0;
			size_t sz = 0;
			bool result = false;
			if (!_dupenv_s(&teamCityProjectName, &sz, "TEAMCITY_PROJECT_NAME")) {
				result = teamCityProjectName != NULL;
				free(teamCityProjectName);
			}

			return result;
#else
			return getenv("TEAMCITY_PROJECT_NAME") != NULL;
#endif
		}

		/// Ctor
		TeamCityAdapter()
			: m_out(&std::cout), m_flowid(getFlowIdFromEnvironment())
		{
		}
		/// Ctor
		/// \param flowid The TeamCity flowid used for messages
		TeamCityAdapter(const std::string& flowid)
			: m_out(&std::cout), m_flowid(flowid)
		{
		}
		virtual ~TeamCityAdapter() = default;
		TeamCityAdapter(const TeamCityAdapter& src) = delete;
		TeamCityAdapter(TeamCityAdapter&& src) = delete;

		inline const std::string& flowid() const
		{
			return m_flowid;
		}

	private:

#if 1 // BEGIN copied from teamcity_messages.cpp
		// https://github.com/JetBrains/teamcity-cpp/blob/master/common/teamcity_messages.cpp
		// changes:
		//  * functions made inline
		//  * `escape` made static
		//  * added `flush` at the end of `testFinished`
		//  * Most functions are private
		//  * `suiteStarted`, `suiteFinished` are public to allow nesting
		static inline std::string escape(const std::string &s) {
			std::string result;
			result.reserve(s.length());

			for (size_t i = 0; i < s.length(); i++) {
				char c = s[i];

				switch (c) {
				case '\n': result.append("|n"); break;
				case '\r': result.append("|r"); break;
				case '\'': result.append("|'"); break;
				case '|':  result.append("||"); break;
				case ']':  result.append("|]"); break;
				default:   result.append(&c, 1);
				}
			}

			return result;
		}

		void inline openMsg(const std::string &name) {
			// endl for http://jetbrains.net/tracker/issue/TW-4412
			*m_out << std::endl << "##teamcity[" << name;
		}

		void inline closeMsg() {
			*m_out << "]";
			// endl for http://jetbrains.net/tracker/issue/TW-4412
			*m_out << std::endl;
		}

		void inline writeProperty(const std::string &name, const std::string &value) {
			*m_out << " " << name << "='" << escape(value) << "'";
		}

	public:

		void inline suiteStarted(const std::string &name, const std::string &flowid) {
			openMsg("testSuiteStarted");
			writeProperty("name", name);
			if (flowid.length() > 0) {
				writeProperty("flowId", flowid);
			}

			closeMsg();
		}

		void inline suiteFinished(const std::string &name, const std::string &flowid) {
			openMsg("testSuiteFinished");
			writeProperty("name", name);
			if (flowid.length() > 0) {
				writeProperty("flowId", flowid);
			}

			closeMsg();
		}

	private:

		void inline testStarted(const std::string &name, const std::string &flowid, bool captureStandardOutput = false) {
			openMsg("testStarted");
			writeProperty("name", name);
			if (flowid.length() > 0) {
				writeProperty("flowId", flowid);
			}

			if (captureStandardOutput) {
				writeProperty("captureStandardOutput", "true"); // false by default
			}

			closeMsg();
		}

		void inline testFinished(const std::string &name, int durationMs, const std::string &flowid) {
			openMsg("testFinished");

			writeProperty("name", name);

			if (flowid.length() > 0) {
				writeProperty("flowId", flowid);
			}

			if (durationMs >= 0) {
				std::stringstream out(std::ios_base::out);
				out << durationMs;
				writeProperty("duration", out.str());
			}

			closeMsg();
			m_out->flush();
		}

		void inline testFailed(const std::string &name, const std::string &message, const std::string &details, const std::string &flowid) {
			openMsg("testFailed");
			writeProperty("name", name);
			writeProperty("message", message);
			writeProperty("details", details);
			if (flowid.length() > 0) {
				writeProperty("flowId", flowid);
			}

			closeMsg();
		}

		void inline testIgnored(const std::string &name, const std::string &message, const std::string &flowid) {
			openMsg("testIgnored");
			writeProperty("name", name);
			writeProperty("message", message);
			if (flowid.length() > 0) {
				writeProperty("flowId", flowid);
			}

			closeMsg();
		}

		void inline testOutput(const std::string &name, const std::string &output, const std::string &flowid, bool isStdError = false) {
			openMsg(isStdError ? "testStdErr" : "testStdOut");
			writeProperty("name", name);
			writeProperty("out", output);
			if (flowid.length() > 0) {
				writeProperty("flowId", flowid);
			}

			closeMsg();
		}

#endif	// END copied from teamcity_messages.cpp

	public:
#if 1 // BEGIN copied from teamcity_gtest.cpp
		// https://github.com/JetBrains/teamcity-cpp/blob/master/gtest/teamcity_gtest.cpp
		// changes:
		//  * functions made inline
		//  * messages member removed
		//  * explicit namespace at `TestCase`
		//  * using `m_flowid`

		// Fired before the test case starts.
		void OnTestCaseStart(const ::testing::TestCase& test_case) {
			suiteStarted(test_case.name(), m_flowid);
		}

		// Fired before the test starts.
		void OnTestStart(const ::testing::TestInfo& test_info) {
			testStarted(test_info.name(), m_flowid);
		}

		// Fired after the test ends.
		void OnTestEnd(const ::testing::TestInfo& test_info) {
			const ::testing::TestResult* result = test_info.result();
			if (result->Failed()) {
				std::string message;
				std::string details;
				for (int i = 0; i < result->total_part_count(); ++i) {
					const ::testing::TestPartResult& partResult = result->GetTestPartResult(i);
					if (partResult.passed()) {
						continue;
					}

					if (message.empty()) {
						message = partResult.summary();
					}

					if (!details.empty()) {
						details.append("\n");
					}
					details.append(partResult.message());

					if (partResult.file_name() && partResult.line_number() >= 0) {
						std::stringstream ss;
						ss << "\n at " << partResult.file_name() << ":" << partResult.line_number();
						details.append(ss.str());
					}
				}

				testFailed(
					test_info.name(),
					!message.empty() ? message : "failed",
					details,
					m_flowid
				);
			}
			testFinished(test_info.name(), static_cast<int>(result->elapsed_time()), m_flowid);
		}

		// Fired after the test case ends.
		void OnTestCaseEnd(const ::testing::TestCase& test_case) {
			suiteFinished(test_case.name(), m_flowid);
		}
#endif	// END copied from teamcity_gtest.cpp
	};

}
