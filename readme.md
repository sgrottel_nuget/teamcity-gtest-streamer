# TeamCity GoogleTest Streamer

This repository contains a small library for better integration of GoogleTest into TeamCity.

On problems contact SGrottel: 

* https://bitbucket.org/sgrottel_nuget/teamcity-gtest-streamer
* https://www.sgrottel.de
* http://go.sgrottel.de/nuget/teamcity-gtest-streamer

## License

This derived work is licensed under Apache License v2.0

See LICENSE.txt for a detailed description.

## Origin

Large portions of this code are based on work by JetBrains:

https://github.com/JetBrains/teamcity-cpp

Their work is licensed under Apache License v2.0

Classes and functions, which were copied are marked accordingly.
